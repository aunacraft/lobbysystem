package net.aunacraft.lobbysystem;

import lombok.Getter;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.api.database.config.impl.SpigotDatabaseConfig;
import net.aunacraft.lobbysystem.config.LobbyConfig;
import net.aunacraft.lobbysystem.player.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class LobbySystem extends JavaPlugin {

    private static LobbySystem instance;
    private LobbyConfig lobbyConfig;
    private PlayerManager playerManager;
    private DatabaseHandler databaseHandler;

    public static LobbySystem getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        Bukkit.getLogger().info("Initializing LobbySystem...");
        instance = this;
        super.onEnable();
        this.databaseHandler = new DatabaseHandler(new SpigotDatabaseConfig(this));
        this.lobbyConfig = new LobbyConfig(this.databaseHandler);
        this.playerManager = new PlayerManager();
        AunaAPI.getApi().getMessageService().applyPrefix("lobbysystem", "lobbysystem.prefix");
        Bukkit.getLogger().info("Initialized successfull!");
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
