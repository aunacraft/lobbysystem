package net.aunacraft.lobbysystem.config;

import lombok.Getter;
import lombok.Setter;
import net.aunacraft.api.bukkit.util.LocationUtil;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.lobbysystem.player.PlayerFlag;
import org.bukkit.Chunk;
import org.bukkit.Location;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
public class LobbyConfig {

    private CopyOnWriteArrayList<PlayerFlag> defaultFlags;
    @Setter private Location spawn;
    @Setter private boolean saveTmp;

    public LobbyConfig(DatabaseHandler databaseHandler) {
        CopyOnWriteArrayList<PlayerFlag> dfFlags = new CopyOnWriteArrayList<>();
        dfFlags.add(PlayerFlag.SURVIVAL);
        this.defaultFlags = dfFlags;
        databaseHandler.createBuilder("CREATE TABLE IF NOT EXISTS lobby_config(key TEXT, value TEXT);");
        databaseHandler.createBuilder("SELECT * FROM lobby_config WHERE key = ?").addObjects("spawn").queryAsync(rs -> {
            try {
                if(rs.next()){
                    spawn = LocationUtil.locationFromString(rs.getString("value"));
                }
            }catch (Exception exception){
                exception.printStackTrace();
            }
        });
    }

    public void saveTemp(){
        if(saveTmp){

        }
    }
}
