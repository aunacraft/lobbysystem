package net.aunacraft.lobbysystem.listeners;

import net.aunacraft.lobbysystem.LobbySystem;
import net.aunacraft.lobbysystem.player.PlayerFlag;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class FlagListener implements Listener {

    public FlagListener() {
        Bukkit.getPluginManager().registerEvents(this, LobbySystem.getInstance());
    }

    @EventHandler
    public void handleBreak(BlockBreakEvent event){
        if(LobbySystem.getInstance().getPlayerManager().getLobbyPlayer(event.getPlayer().getName()).hasFlag(PlayerFlag.BREAK)){
            event.setCancelled(false);
        }else{
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void handlePlace(BlockPlaceEvent event){
        if(LobbySystem.getInstance().getPlayerManager().getLobbyPlayer(event.getPlayer().getName()).hasFlag(PlayerFlag.BUILD)){
            event.setCancelled(false);
        }else{
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void handleInteract(PlayerInteractEvent event){
        if(event.getAction().equals(Action.PHYSICAL)){
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void handleEntityInteract(PlayerInteractEntityEvent event){
        if(LobbySystem.getInstance().getPlayerManager().getLobbyPlayer(event.getPlayer().getName()).hasFlag(PlayerFlag.BUILD)){
            event.setCancelled(false);
        }else{
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void handleEvE(EntityDamageByEntityEvent event){
        if(LobbySystem.getInstance().getPlayerManager().getLobbyPlayer(((Player) event.getDamager()).getName()).hasFlag(PlayerFlag.PVE)){
            event.setCancelled(false);
        }else{
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void handleFlightToggle(PlayerToggleFlightEvent event){
        if(LobbySystem.getInstance().getPlayerManager().getLobbyPlayer(event.getPlayer().getName()).hasFlag(PlayerFlag.DOUBLE_JUMP)){
            event.setCancelled(true);
            Player player = event.getPlayer();
            player.setFlying(false);
            player.setAllowFlight(false);

            player.setVelocity(player.getLocation().getDirection().multiply(1.0D * 2).setY(1.0D * 2));
            player.getWorld().playSound(player.getLocation(), Sound.ENTITY_BAT_LOOP, 1.0F, 5.0F);
            player.setFallDistance(0.0F);
        }else{
            if(LobbySystem.getInstance().getPlayerManager().getLobbyPlayer(event.getPlayer().getName()).hasFlag(PlayerFlag.FLY)){
                event.setCancelled(false);
            }else{
                event.setCancelled(true);
            }
        }
    }

}
