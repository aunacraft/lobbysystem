package net.aunacraft.lobbysystem.command;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.lobbysystem.LobbySystem;
import net.aunacraft.lobbysystem.config.LobbyConfig;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class SetupCommand implements CommandExecutor {

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        if(commandSender instanceof Player){
            Player player = ((Player) commandSender).getPlayer();
            AunaPlayer aunaPlayer = AunaAPI.getApi().getPlayer(player.getUniqueId());
            if(player.hasPermission("lobbysystem.setup")){
                player.sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(player, "lobbysystem.noperms"));
                return true;
            }
            if(args.length == 0){
                player.sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(player, "lobbysystem.command.setup.usage"));
                return true;
            }
            if(args.length == 1){
                if(args[0].equalsIgnoreCase("spawn")){
                    LobbySystem.getInstance().getLobbyConfig().setSpawn(aunaPlayer.toBukkitPlayer().getLocation());
                    LobbySystem.getInstance().getLobbyConfig().setSaveTmp(true);
                }
            }else{
                player.sendMessage(AunaAPI.getApi().getMessageService().getMessageForPlayer(player, "lobbysystem.command.setup.usage"));
            }
        }else{
            commandSender.sendMessage("§cYou have to execute this Command as a Player! :)");
        }
        return false;
    }
}
