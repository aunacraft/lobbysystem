package net.aunacraft.lobbysystem.player;

public enum PlayerFlag {

    BUILD, PVE, BREAK, ADVENTURE, SURVIVAL, CREATIVE, FLY, DOUBLE_JUMP;

}
