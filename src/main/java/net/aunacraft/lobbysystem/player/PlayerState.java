package net.aunacraft.lobbysystem.player;

import lombok.Getter;
import org.bukkit.inventory.ItemStack;

public enum PlayerState {

    NONE(null),
    JUMP_AND_RUN(null);

    @Getter private ItemStack icon;

    PlayerState(ItemStack icon) {
        this.icon = icon;
    }
}
