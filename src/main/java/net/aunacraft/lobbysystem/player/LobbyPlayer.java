package net.aunacraft.lobbysystem.player;

import lombok.Getter;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.player.impl.SpigotAunaPlayer;
import net.aunacraft.lobbysystem.LobbySystem;

import java.util.List;

public class LobbyPlayer extends SpigotAunaPlayer {

    private List<PlayerFlag> playerFlag;
    @Getter private PlayerState playerState;

    public LobbyPlayer(AunaPlayer player, List<PlayerFlag> playerFlag, PlayerState playerState) {
        super(player.getUuid(), player.getOfflineUUID(), player.getName(), player.getId(), player.getIp(), player.getIpHistory(),
                player.getLogins(), player.getLastLogin(), player.getFirstLogin(), player.getPlayerMeta(), player.getCoins(),
                player.getBankCoins(), player.getLevel(), player.getLevelXP(), player.getMessageLanguage());
        this.playerFlag = playerFlag;
        this.playerState = playerState;
    }

    public void handleRegister(){
        this.toBukkitPlayer().teleport(LobbySystem.getInstance().getLobbyConfig().getSpawn());

    }

    public boolean hasFlag(PlayerFlag flag){
        return playerFlag.contains(flag);
    }

    public void handleUnregister(){

    }


}
