package net.aunacraft.lobbysystem.player;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.event.impl.PlayerRegisterEvent;
import net.aunacraft.api.event.impl.PlayerUnregisterEvent;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.lobbysystem.LobbySystem;

import java.util.concurrent.CopyOnWriteArrayList;

public class PlayerManager {

    private CopyOnWriteArrayList<LobbyPlayer> registeredPlayers;

    public PlayerManager() {
        this.registeredPlayers = new CopyOnWriteArrayList<>();

        AunaAPI.getApi().registerEventListener(PlayerRegisterEvent.class, this::handleRegister);
        AunaAPI.getApi().registerEventListener(PlayerUnregisterEvent.class, this::handleUnregister);
    }

    public LobbyPlayer getLobbyPlayer(AunaPlayer player){
        for (LobbyPlayer players : registeredPlayers){
            if(players.getUuid().toString().equalsIgnoreCase(player.getUuid().toString())){
                return players;
            }
        }
        return null;
    }

    public LobbyPlayer getLobbyPlayer(String name){
        for (LobbyPlayer players : registeredPlayers){
            if(players.getName().equalsIgnoreCase(name)){
                return players;
            }
        }
        return null;
    }

    public void register(LobbyPlayer player){
        if(!registeredPlayers.contains(player)){
            registeredPlayers.add(player);
            player.handleRegister();
        }
    }

    public void unregister(LobbyPlayer player){
        if(registeredPlayers.contains(player)){
            registeredPlayers.remove(player);
            player.handleUnregister();
        }
    }

    public void handleRegister(PlayerRegisterEvent event){
        register(new LobbyPlayer(event.getPlayer(), LobbySystem.getInstance().getLobbyConfig().getDefaultFlags(), PlayerState.NONE));
    }

    public void handleUnregister(PlayerUnregisterEvent event){
        unregister(this.getLobbyPlayer(event.getPlayer()));
    }

}
